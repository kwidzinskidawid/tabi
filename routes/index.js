var mongoose = require('mongoose');

var JournalEntry = require('../models/JournalEntries.js');
var Message = require('../models/Messages.js');
var Place = require('../models/Places.js');
var PlanEntry = require('../models/PlanEntries.js');
var Rating = require('../models/Ratings.js');
var User = require('../models/Users.js');

var express = require('express');
var _ = require('underscore');
var router = express.Router();


//////////////////////////////////////////////////////
//JOURNAL ENTRY	

router.get('/journalEntries/getByIDwithPlaces/:entryId', function (req, res) {
	JournalEntry
		.findOne({_id: req.params.entryId})
		.populate('placeID ratings')
		.exec(function (err, journalEntry) {
			if (err) {
				res.end(err);
			}
			res.json(journalEntry);
		});
});	

router.get('/journalEntries/getByUserWithPlaces/:userId', function (req, res) {
	JournalEntry
		.find({userID: req.params.userId})
		.populate('placeID')
		.exec(function (err, journalEntries) {
			if (err) {
				res.end(err);
			}
			res.json(journalEntries);
		});
});	

router.get('/journalEntries/getByPlaceNameWithRatings/:placeName', function (req, res) {
	JournalEntry
		.find({})
		.populate('placeID ratings')
		.exec(function (err, journalEntries) {
			if (err) {
				res.end(err);
			}
			var results = _.filter(journalEntries, function (entry) {
				return entry.placeID.name === req.params.placeName;
			});
			res.json(results);
		});
});	
 	
router.get('/journalEntries/getByPlace/:id', function (req, res) {
	JournalEntry.find({placeID: req.params.id}, function (err, journalEntries) {
		if (err) {
			res.end(err);
		}

		res.json(journalEntries);
	});
});

router.get('/journalEntries/getByCountryCode/:code', function (req, res) {
	JournalEntry
		.find({})
		.populate('placeID')
		.exec(function (err, journalEntries) {
			if (err) {
				res.end(err);
			}
			var results = _.filter(journalEntries, function (entry) {
				return entry.placeID.countryCode === req.params.code;
			});

			res.json(results);
		});
});		

router.post('/journalEntries/add', function (req, res) {
	var newJournalEntry = new JournalEntry(req.body);

	newJournalEntry.save(function (err, item){
	    if (err){
	        console.log(err);
	    	res.end("ERROR");
	    } else {
	        console.log("New journal entry added");
	    	res.end(item._id.toString());
	    }
	});
});

router.get('/journalEntries/get/:id', function (req, res) {
	JournalEntry.findById(req.params.id, function (err, result) {
		if (err) {
			res.end(err);
		} else {
			res.json(result);
		}
	});
});

router.get('/journalEntries/get', function (req, res) {
	JournalEntry.find({}, function (err, results) {
		if (err) {
			res.end(err);
		} else {
    		res.json(results);
   		}
	});
});

router.put('/journalEntries/update/:id', function (req, res) {
	var changedJournalEntry = req.body;
	JournalEntry.findByIdAndUpdate(req.params.id, changedJournalEntry, function (err) {
		 if (err){
	    	res.end("ERROR");
	    } else {
	    	res.end("OK");
	    }
	});
});

router.delete('/journalEntries/delete/:id', function (req, res) {
	JournalEntry.findByIdAndRemove(req.params.id, function (err) {
		 if (err){
	    	res.end("ERROR");
	    } else {
	    	res.end("OK");
	    }
	});
});

//////////////////////////////////////////////////////
//MESSAGES	 			

router.get('/messages/getByUser/:userId', function (req, res) {
	Message
		.find({receiverID: req.params.userId})
		.populate('senderID')
		.exec(function (err, results) {
			if (err) {
				res.end(err);
			}
				res.json(results);
		});
});

router.post('/messages/add', function (req, res) {
	var newMessage = new Message(req.body);

	newMessage.save(function (err){
	    if (err){
	        console.log(err);
	    	res.end("ERROR");
	    } else {
	        console.log("New message added");
	    	res.end("OK");
	    }
	});
});

router.get('/messages/get/:id', function (req, res) {
	Message.findById(req.params.id, function (err, result) {
		if (err) {
			res.end(err);
		} else {
			res.json(result);
		}
	});
});

router.get('/messages/get', function (req, res) {
	Message.find({}, function (err, results) {
		if (err) {
			res.end(err);
		} else {
    		res.json(results);
   		}
	});
});

router.put('/messages/update/:id', function (req, res) {
	var changedMessage = req.body;
	Message.findByIdAndUpdate(req.params.id, changedMessage, function (err) {
		 if (err){
	    	res.end("ERROR");
	    } else {
	    	res.end("OK");
	    }
	});
});

router.delete('/messages/delete/:id', function (req, res) {
	Message.findByIdAndRemove(req.params.id, function (err) {
		 if (err){
	    	res.end("ERROR");
	    } else {
	    	res.end("OK");
	    }
	});
});

//////////////////////////////////////////////////////
//PLACES 	

router.get('/places/getByPlaceName/:name', function (req, res)  {
	Place.findOne({name: new RegExp('^'+req.params.name+'$', "i")}, function (err, result) {
		if (err) {
			res.end(err);
		} 
		res.json(result);
	});
});	

router.get('/places/getByCountryName/:name', function (req, res)  {
	Place.find({country: req.params.name}, function (err, results) {
		if (err) {
			res.end(err);
		} 
		res.json(results);
	});
});	

router.put('/places/getByCoords', function (req, res) {
	Place.findOne({
		latitude: req.body.lat,
	 	longitude: req.body.lng
	}, function (err, result) {
		if (err){
	        console.log(err);
	    	res.end("ERROR");
    	} 
        if(result === null) {
        	res.end("not found");
        } else {
        	res.end(result._id.toString());
        }
    	
	    
	});
});

router.get('/places/getByCountryCode/:code', function (req, res) {
	Place.find({countryCode: req.params.code}, function (err, results) {
		if (err) {
			res.end(err);
		} else {
    		res.json(results);
   		}
	});
});

router.post('/places/add', function (req, res) {
	var newPlace = new Place(req.body);

	newPlace.save(function (err, doc){
	    if (err){
	        console.log(err);
	    	res.end("ERROR");
	    } else {
	        console.log("New place entry added");
	    	res.end(doc._id.toString());
	    }
	});
});

router.get('/places/get/:id', function (req, res) {
	console.log("id here: " + req.params.id);
	Place.findById(req.params.id, function (err, result) {
		if (err) {
			res.end(err);
		} else {
			res.json(JSON.stringify(result));
		}
	});
});

router.get('/places/get', function (req, res) {
	Place.find({}, function (err, results) {
		if (err) {
			res.end(err);
		} else {
    		res.json(results);
   		}
	});
});

router.put('/places/update/:id', function (req, res) {
	var changedPlace = req.body;
	Place.findByIdAndUpdate(req.params.id, changedPlace, function (err) {
		 if (err){
	    	res.end("ERROR");
	    } else {
	    	res.end("OK");
	    }
	});
});

router.delete('/places/delete/:id', function (req, res) {
	Place.findByIdAndRemove(req.params.id, function (err) {
		 if (err){
	    	res.end("ERROR");
	    } else {
	    	res.end("OK");
	    }
	});
});

//////////////////////////////////////////////////////
//PLAN ENTRIES 			

router.get('/planEntries/getByUserWithPlaces/:id', function (req, res) {
	PlanEntry
	.find({userID: req.params.id})
	.populate('placeID')
	.exec(function (err, results) {
		if (err) {
			res.end(err);
		} else {
			res.json(results);
		}
	});
});

router.post('/planEntries/add', function (req, res) {
	var newPlanEntry = new PlanEntry(req.body);

	newPlanEntry.save(function (err){
	    if (err){
	        console.log(err);
	    	res.end("ERROR");
	    } else {
	        console.log("New plan entry added");
	    	res.end("OK");
	    }
	});
});

router.get('/planEntries/get/:id', function (req, res) {
	PlanEntry.findById(req.params.id, function (err, result) {
		if (err) {
			res.end(err);
		} else {
			res.json(result);
		}
	});
});

router.get('/planEntries/get', function (req, res) {
	PlanEntry.find({}, function (err, results) {
		if (err) {
			res.end(err);
		} else {
    		res.json(results);
   		}
	});
});

router.put('/planEntries/update/:id', function (req, res) {
	var changedPlanEntry = req.body;
	PlanEntry.findByIdAndUpdate(req.params.id, changedPlanEntry, function (err) {
		 if (err){
	    	res.end("ERROR");
	    } else {
	    	res.end("OK");
	    }
	});
});

router.delete('/planEntries/delete/:id', function (req, res) {
	PlanEntry.findByIdAndRemove(req.params.id, function (err) {
		 if (err){
	    	res.end("ERROR");
	    } else {
	    	res.end("OK");
	    }
	});
});

//////////////////////////////////////////////////////
//RATINGS	

router.get('/ratings/getByCountryCode/:code', function (req, res) {
	var result = [];
	Rating
		.find({})
		.populate('placeID').
		exec(function (err, results) {
			if (err) {
				res.end(err);
			}

			_.each(results, function (el) {
				if(el.placeID.countryCode === req.params.code) {
					result.push(el);
				}
			});
			res.json(result);
		});

});		

router.get('/ratings/getByPlace/:id', function (req, res) {
	Rating.find({placeID: req.params.id}, function (err, result) {
		if (err) {
			res.end(err);
		} else {
			res.json(result);
		}
	});
});

router.post('/ratings/add', function (req, res) {
	var newRating = new Rating(req.body);
	            
	newRating.save(function (err, item){
	    if (err){
	        console.log(err);
	    	res.end("ERROR");
	    } else {
	        console.log("New rating added");
	    	res.end(item._id.toString());
	    }
	});
});

router.get('/ratings/get/:id', function (req, res) {
	Rating.findById(req.params.id, function (err, result) {
		if (err) {
			res.end(err);
		} else {
			res.json(result);
		}
	});
});

router.get('/ratings/get', function (req, res) {
	Rating.find({}, function (err, results) {
		if (err) {
			res.end(err);
		} else {
    		res.json(results);
   		}
	});
});

router.put('/ratings/update/:id', function (req, res) {
	var changedRating = req.body;
	Rating.findByIdAndUpdate(req.params.id, changedRating, function (err) {
		 if (err){
	    	res.end("ERROR");
	    } else {
	    	res.end("OK");
	    }
	});
});

router.delete('/ratings/delete/:id', function (req, res) {
	Rating.findByIdAndRemove(req.params.id, function (err) {
		 if (err){
	    	res.end("ERROR");
	    } else {
	    	res.end("OK");
	    }
	});
});


//////////////////////////////////////////////////////
//USER 			

router.post('/login', function (req, res) {
	User.findOne({login: req.body.login, password: req.body.password}, function (err, result) {
		if (err) {
			res.end(err);
		} 
		if (result === null) {
			res.json({err:"notFound"});
		} else {
			res.json({ID: result._id, sessionID: req.sessionID});
		}
		
	});
});

router.post('/users/add', function (req, res) {
	var newUser = new User(req.body);

	newUser.save(function (err, result){
	    if (err){
	        console.log(err);
	    	res.json({err:"couldntCreate"});
	    } else {
	        console.log("New user added");
	    	res.json({ID: result._id, sessionID: req.sessionID});
	    }
	});
});

router.get('/users/getByLogin/:login', function (req, res) {
	User.findOne({login: req.params.login}, function (err, result) {
		if (err) {
			res.end(err);
		} else {
			res.json(result);
		}
	});
});

router.get('/users/get/:id', function (req, res) {
	User.findById(req.params.id, function (err, result) {
		if (err) {
			res.end(err);
		} else {
			res.json(result);
		}
	});
});

router.get('/users/get', function (req, res) {
	User.find({}, function (err, results) {
		if (err) {
			res.end(err);
		} else {
    		res.json(results);
   		}
	});
});

router.put('/users/update/:id', function (req, res) {
	var changedUser = req.body;
	User.findByIdAndUpdate(req.params.id, changedUser, function (err) {
		 if (err){
	    	res.end("ERROR");
	    } else {
	    	res.end("OK");
	    }
	});
});

router.delete('/users/delete/:id', function (req, res) {
	User.findByIdAndRemove(req.params.id, function (err) {
		 if (err){
	    	res.end("ERROR");
	    } else {
	    	res.end("OK");
	    }
	});
});

//////////////////////////////////////////////////
//export router
module.exports = router;
