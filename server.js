/* jshint node: true */
var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var favicon = require('serve-favicon');
var less = require('less-middleware');
var session = require('express-session');


////////////////////////
//files
var multer = require('multer')
  , storage = multer.diskStorage({
	    destination: function (req, file, cb) {
	        cb(null, 'photos/')
	    },
	    filename: function (req, file, cb) {
	        cb(null, req.body.flowIdentifier);
	    }
    })
  , multerUpload = multer({ storage: storage })
  , uploadFile = multerUpload.single('file');


///////////////////////
//db
var mongoose = require('mongoose');
//mongoose.connect('tabi.cloudapp.net:33333/tabi');
mongoose.connect('localhost:27017/tabi');


var routes = require('./routes/index.js');

var app = express();

app.use('/lib/', express.static(path.join(__dirname, '/bower_components/jquery/dist/')));
app.use('/lib/', express.static(path.join(__dirname, '/bower_components/raphael/')));
app.use('/lib/', express.static(path.join(__dirname, '/bower_components/neveldo/jQuery-Mapael/js/')));
app.use('/lib/', express.static(path.join(__dirname, '/bower_components/jquery-mousewheel/')));
app.use('/lib/', express.static(path.join(__dirname, '/bower_components/angular/')));
app.use('/lib/', express.static(path.join(__dirname, '/bower_components/angular-ui-router/release/')));
app.use('/lib/', express.static(path.join(__dirname, '/bower_components/angular-toastr/dist/')));
app.use('/lib/', express.static(path.join(__dirname, '/bower_components/angular-spinner/')));
app.use('/lib/', express.static(path.join(__dirname, '/bower_components/spin.js/')));
app.use('/lib/', express.static(path.join(__dirname, '/bower_components/async/dist/')));
app.use('/lib/', express.static(path.join(__dirname, '/bower_components/underscore/')));
app.use('/lib/', express.static(path.join(__dirname, '/bower_components/ng-flow/dist/')));

app.use(less(path.join(__dirname, '/app')));
app.use(express.static(path.join(__dirname, '/app')));


app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(favicon(path.join(__dirname + '/app/webicon.ico')));


app.use(session({
  secret: 'dl2@3a1l',
  resave: false,
  saveUninitialized: false
}));

app.use('/rest/', routes);


// app.use(function(req, res, next) {
//     res.setHeader('Access-Control-Allow-Origin', '*');
//     res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
//     next();
// });

app.get('/rest/getPhoto/:id', function(req, res){
var options = {
    root: __dirname + '/photos/',
    dotfiles: 'deny',
    headers: {
        'x-timestamp': Date.now(),
        'x-sent': true
    }
  };

  var fileName = req.params.id;
  res.sendFile(fileName, options, function (err) {
    if (err) {
      console.log(err);
      res.status(err.status).end();
    }
    else {
      console.log('Sent:', fileName);
    }
  });
});

app.post('/rest/upload', uploadFile, function(req, res) {
	res.status(200).send();
});

app.listen(3000, function () {
	console.log("Server is listening on port 3000");
});