
tabiApp.factory('Session', function () {
	var obj = {};

	obj.create = function (userID, sessionID) {
	    obj.id = sessionID;
	    obj.userID = userID;
	    console.log("Session created");
	};
	obj.destroy = function () {
	    obj.id = undefined;
	    obj.userID = undefined;
	};

	return obj;
});


tabiApp.factory('journalEntries', function ($http) {
	var getByIDwithPlaces = function (entryId) {
		return $http.get('/rest/journalEntries/getByIDwithPlaces/' + entryId);
	};
	var getByUserWithPlaces = function (userId) {
		return $http.get('/rest/journalEntries/getByUserWithPlaces/' + userId);
	};
	var getByPlaceNameWithRatings = function (placeName) {
		return $http.get('/rest/journalEntries/getByPlaceNameWithRatings/' + placeName);
	};	
	var getByPlace = function (placeId) {
		return $http.get('/rest/journalEntries/getByPlace/' + placeId);
	};
	var getByCountryCode = function (countryCode) {
		return $http.get('/rest/journalEntries/getByCountryCode/' + countryCode);
	};
	var get = function (id) {
		return $http.get('/rest/journalEntries/get/' + id);
	};
	var getAll = function () {
		return $http.get('/rest/journalEntries/get');
	};
	var update = function (id, changedItem) {
		return $http.put('/rest/journalEntries/update/' + id, changedItem);
	};
	var remove = function (id) {
		return $http.delete('/rest/journalEntries/delete/' + id);
	};
	var add = function (item) {
		return $http.post('/rest/journalEntries/add', item);
	};

	return {
		getByIDwithPlaces: getByIDwithPlaces,
		getByUserWithPlaces: getByUserWithPlaces,
		getByPlaceNameWithRatings: getByPlaceNameWithRatings,
		getByPlace: getByPlace,
		getByCountryCode: getByCountryCode,
		get: get,
		getAll: getAll,
		update: update,
		remove: remove,
		add: add
	};
});

tabiApp.factory('messages', function ($http) {
	var getByUser = function (userId) {
		return $http.get('/rest/messages/getByUser/' + userId);
	};
	var get = function (id) {
		return $http.get('/rest/messages/get/' + id);
	};
	var getAll = function () {
		return $http.get('/rest/messages/get');
	};
	var update = function (id, changedItem) {
		return $http.put('/rest/messages/update/' + id, changedItem);
	};
	var remove = function (id) {
		return $http.delete('/rest/messages/delete/' + id);
	};
	var add = function (item) {
		return $http.post('/rest/messages/add', item);
	};

	return {
		getByUser: getByUser,
		get: get,
		getAll: getAll,
		update: update,
		remove: remove,
		add: add
	};
});

tabiApp.factory('places', function ($http) {
	var getByPlaceName = function (placeName) {
		return $http.get('/rest/places/getByPlaceName/' + placeName);
	};
	var getByCountryName = function (countryName) {
		return $http.get('/rest/places/getByCountryName/' + countryName);
	};
	var getByCoords = function (lat, lng) {
		return $http.put('/rest/places/getByCoords', {lat: lat, lng: lng});
	};
	var getByCountryCode = function (countryCode) {
		return $http.get('/rest/places/getbyCountryCode/' + countryCode);
	};
	var get = function (id) {
		return $http.get('/rest/places/get/' + id);
	};
	var getAll = function () {
		return $http.get('/rest/places/get');
	};
	var update = function (id, changedItem) {
		return $http.put('/rest/places/update/' + id, changedItem);
	};
	var remove = function (id) {
		return $http.delete('/rest/places/delete/' + id);
	};
	var add = function (item) {
		return $http.post('/rest/places/add', item);
	};

	return {
		getByPlaceName: getByPlaceName,
		getByCountryName: getByCountryName,
		getByCoords: getByCoords,
		getByCountryCode: getByCountryCode,
		get: get,
		getAll: getAll,
		update: update,
		remove: remove,
		add: add
	};
});

tabiApp.factory('planEntries', function ($http) {
	var getByUserWithPlaces = function (userId) {
		return $http.get('/rest/planEntries/getByUserWithPlaces/' + userId);
	};
	var get = function (id) {
		return $http.get('/rest/planEntries/get/' + id);
	};
	var getAll = function () {
		return $http.get('/rest/planEntries/get');
	};
	var update = function (id, changedItem) {
		return $http.put('/rest/planEntries/update/' + id, changedItem);
	};
	var remove = function (id) {
		return $http.delete('/rest/planEntries/delete/' + id);
	};
	var add = function (item) {
		return $http.post('/rest/planEntries/add', item);
	};

	return {
		getByUserWithPlaces: getByUserWithPlaces,
		get: get,
		getAll: getAll,
		update: update,
		remove: remove,
		add: add
	};
});

tabiApp.factory('ratings', function ($http) {
	var getByCountryCode = function (countryCode) {
		return $http.get('/rest/ratings/getByCountryCode/' + countryCode);
	};
	var getByPlace = function (placeId) {
		return $http.get('/rest/ratings/getByPlace/' + placeId);
	};
	var get = function (id) {
		return $http.get('/rest/ratings/get/' + id);
	};
	var getAll = function () {
		return $http.get('/rest/ratings/get');
	};
	var update = function (id, changedItem) {
		return $http.put('/rest/ratings/update/' + id, changedItem);
	};
	var remove = function (id) {
		return $http.delete('/rest/ratings/delete/' + id);
	};
	var add = function (item) {
		return $http.post('/rest/ratings/add', item);
	};

	return {
		getByCountryCode: getByCountryCode,
		getByPlace: getByPlace,
		get: get,
		getAll: getAll,
		update: update,
		remove: remove,
		add: add
	};
});

tabiApp.factory('users', function ($http){
	var login = function (user) {
		return $http.post('/rest/login', user);
	};
	var getByLogin = function (login) {
		return $http.get('/rest/users/getByLogin/' + login);
	};
	var get = function (id) {
		return $http.get('/rest/users/get/' + id);
	};
	var getAll = function () {
		return $http.get('/rest/users/get');
	};
	var update = function (id, changedItem) {
		return $http.put('/rest/users/update/' + id, changedItem);
	};
	var remove = function (id) {
		return $http.delete('/rest/users/delete/' + id);
	};
	var add = function (item) {
		return $http.post('/rest/users/add', item);
	};

	return {
		login: login,
		getByLogin: getByLogin,
		get: get,
		getAll: getAll,
		update: update,
		remove: remove,
		add: add
	};
});