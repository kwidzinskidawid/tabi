'use strict';

/**
 * @ngdoc overview
 * @name tabiApp
 * @description
 * # tabiApp
 *
 * Main module of the application.
 */
var tabiApp = angular.module('tabiApp', ['ui.router', 'toastr', 'flow', 'angularSpinner']);

tabiApp.config(['$stateProvider','$urlRouterProvider', '$locationProvider',
 function ($stateProvider, $urlRouterProvider, $locationProvider) {
  $stateProvider
    .state('home', {
      url: '/',
      templateUrl: '/views/main.html',
      controller: 'MainCtrl'
    })
    .state('addPlace', {
      url: '/place/new',
      templateUrl: '/views/addPlace.html',
      controller: 'AddPlaceCtrl'
    })
      .state('addPlace.journal', {
        url: '/journal',
        templateUrl: '/views/addPlaceJournal.html',
        controller: 'AddPlaceJournalCtrl',
        authenticate: true
      })
      .state('addPlace.plan', {
        url: '/plan',
        templateUrl: '/views/addPlacePlan.html',
        controller: 'AddPlacePlanCtrl',
        authenticate: true
      })
    .state('country', {
      url: '/country/:name',
      templateUrl: '/views/country.html',
      controller: 'CountryCtrl'
    })
    .state('place', {
      url: '/place/:name',
      templateUrl: '/views/place.html',
      controller: 'PlaceCtrl'
    })
    .state('profile', {
      url: '/profile',
      templateUrl: '/views/profile.html',
      controller: 'ProfileCtrl',
      authenticate: true
    })
    .state('journal', {
      url: '/journal',
      templateUrl: '/views/journal.html',
      controller: 'JournalCtrl',
      authenticate: true
    })
    .state('journalEntry', {
      url: '/journalEntry/:id',
      templateUrl: '/views/journalEntry.html',
      controller: 'JournalEntryCtrl',
      authenticate: true
    })
    .state('plan', {
      url: '/plan',
      templateUrl: '/views/plan.html',
      controller: 'PlanCtrl',
      authenticate: true
    })
    .state('messages', {
      url: '/messages',
      templateUrl: '/views/messages.html',
      controller: 'MessagesCtrl',
      authenticate: true
    })
    .state('topPlaces', {
      url: '/topPlaces',
      templateUrl: '/views/topPlaces.html',
      controller: 'TopPlacesCtrl'
    });

	$urlRouterProvider.otherwise('/');
	
	//removing # from the url
	// if(window.history && window.history.pushState){
	// 	$locationProvider.html5Mode({
	// 		 enabled: true,
	// 		 requireBase: false
	// 		});
	// 	}
 }
]);


tabiApp.run(function ($rootScope, $state, Session, toastr) {
  $rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams){
    if (toState.authenticate && Session.id === undefined){
      // User isn’t authenticated
      toastr.error('You need to be logged in to see this content.');
      $state.transitionTo("home");
      event.preventDefault(); 
    }
  });
});

tabiApp.config(function(toastrConfig) {
  angular.extend(toastrConfig, {
    extendedTimeOut: 1000,
    containerId: 'toast-container',
    maxOpened: 0,    
    newestOnTop: true,
    positionClass: 'toast-top-full-width',
    preventDuplicates: false,
    preventOpenDuplicates: false,
    target: 'body'
  });
});

tabiApp.config(['flowFactoryProvider', function (flowFactoryProvider) {
    flowFactoryProvider.defaults = {
      target: 'http://localhost:3000/rest/upload',
      //target: 'http://tabi.cloudapp.net/rest/upload',
      permanentErrors: [404, 500, 501],
      chunkRetryInterval: 5000,
      simultaneousUploads: 4,
        testChunks: false,
        generateUniqueIdentifier: function(file) {
            var getFileExt = function(fileName) {
                var fileExt = fileName.split(".");

                if ( fileExt.length === 1 || ( fileExt[0] === "" && fileExt.length === 2 )) {
                    return "";
                }

                return fileExt.pop();
            };

            return Date.now() + '.'+ getFileExt(file.name);
        }
    };
}]);
