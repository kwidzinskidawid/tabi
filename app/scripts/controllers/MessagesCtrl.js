tabiApp.controller('MessagesCtrl', function ($scope, $rootScope, messages, users, Session, toastr) {
	$rootScope.header = "Messages";

	$scope.messages = [];
	$scope.newMessage = {};

	messages.getByUser(Session.userID)
		.success(function (data) {
			_.each(data, function (el, ind) {
				data[ind].date = new Date(data[ind].date).toISOString().slice(0, 10);
			});
			$scope.messages = data;
		});

	$scope.sendMessage = function () {
		users.getByLogin($scope.newMessage.receiver)
			.success(function (data) {
				console.log(data);
				if (data !== null) {
					messages.add({
						receiverID: data._id,
						senderID: Session.userID,
						content: $scope.newMessage.content,
						date: new Date()
					})
						.success(function (data) {
							if (data === "OK") {
								toastr.success('Message has been sent.');
								$scope.newMessage.content = "";
								$scope.newMessage.receiver = "";
								$scope.$apply();
							} else {
								toastr.error('Error while sending the message.');
							}
						});
				} else {
					toastr.error('User with this name does not exist.');
				}
			});
	};
});