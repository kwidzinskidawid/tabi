tabiApp
  .controller('AddPlaceCtrl', function ($scope, places) {

    $scope.place = {};

    var autocomplete = new google.maps.places.Autocomplete(
    (document.getElementById('searchBox')), {
        types: ['(cities)']
        
    });
    autocomplete.addListener('place_changed', fillForm);


    function fillForm() {
      var place = autocomplete.getPlace();

      if(place.geometry !== undefined) {
        $scope.place.latitude = place.geometry.location.lat();
        $scope.place.longitude = place.geometry.location.lng();

        $scope.place.name = place.adr_address;

        //console.log(place.address_components);
        for (var i = 0; i < place.address_components.length; i++) {
          if(place.address_components[i].types[0] === "locality" || place.address_components[i].types[1] === "locality") {
            $scope.place.name = place.address_components[i].short_name;
          }
          if(place.address_components[i].types[0] === "country") {
            $scope.place.country = place.address_components[i].long_name;
            $scope.place.countryCode = place.address_components[i].short_name;
          }
        }
    
        $scope.$apply();
      } else {
        console.log("NO PLEJSU");
      }
    };
});
