tabiApp.controller('JournalCtrl', function ($scope, journalEntries, Session, $state, $rootScope) {
	$rootScope.header = "Journal";
	$scope.entries = [];
	$scope.photos = [];
	$scope.photoToShow = undefined;

	journalEntries.getByUserWithPlaces(Session.userID)
		.success(function (data) {
			$scope.entries = data;
			_.each($scope.entries, function(el, ind) {
				$scope.entries[ind].dateOfJourney = new Date(el.dateOfJourney).toISOString().slice(0, 10);
				$scope.photos.push(el.photos);
				$scope.photos = _.flatten($scope.photos);
			})
		});


	$scope.redirect = function (entryId) {
		$state.go('journalEntry', {id: entryId});
	};

	$scope.showPhoto = function (photo) {
		$scope.photoToShow = photo;
		$('.photoShowWrapper').fadeIn();
	};

	$scope.hidePhoto = function () {
		$('.photoShowWrapper').fadeOut();
	};
});