'use strict';

/**
 * @ngdoc function
 * @name tabiApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the tabiApp
 */
tabiApp
  .controller('MainCtrl', function ($scope, places, journalEntries, $state, $rootScope) {
  	$rootScope.header = "Tabi";
  	$scope.countryInfo = {};

  	var lastSelected;

  	$(".mapcontainer").mapael({
	    map : {
	        name : "world_countries",
	        zoom : {
                enabled : true,
                maxLevel : 7
            },
	        defaultArea : {
	        	attrs: {
	        		fill : "#525252",
	        		stroke : "white",
	        		cursor: "pointer"
	        	},
	        	attrsHover : {
	        		fill : Raphael.getColor()
	        	},
	        	eventHandlers: {
	        		'dblclick': function (e, id, mapElem, textElem, elemOptions) {
	        			$state.go('country', {name: elemOptions.countryName});
	        		},
					'click': function (e, id, mapElem, textElem, elemOptions) {
						$scope.countryInfo.id = id;
						$scope.countryInfo.name = elemOptions.countryName;
						places.getByCountryCode(id)
							.success(function(data) {
								$scope.countryInfo.places = data.length;
							});
						journalEntries.getByCountryCode(id)
							.success(function(data) {
								$scope.countryInfo.visited = data.length;
							});
						
						$scope.$apply();
						
						//if selecting new country
						if (mapElem.originalAttrs.fill === "#525252") {
							$('#countryInfo').animate({width:'show'},600);
							
							if ( lastSelected !== undefined) {
								var newData = {
                            		'areas': {}
	                            };
	                            //clear last selected
	                            newData.areas[lastSelected] = {
	                                attrs: {
	                                    fill: "#525252"
	                                }
	                            };
	                            //add color for new selected
	                            newData.areas[id] = {
	                                attrs: {
	                                    fill: Raphael.getColor()
	                                }
	                            };
	                            
	                            //update colors on the map
	                            $(".map").trigger('update', [newData]);
							}
							
							lastSelected = id;
							mapElem.originalAttrs.fill = "#07a1b2";

						} else {
							$('#countryInfo').animate({width:'hide'},600);
							mapElem.originalAttrs.fill = "#525252";
						}
					}
				}
	        }
	    },
	    areas :  (function() {
	        	var result = {};
	        	for (var country in allCountries) {
	        		result[allCountries[country].id] = {
	        			countryName: allCountries[country].name,
	        			attrsHover: {
			        		fill : Raphael.getColor()
			        	},
						tooltip: {
							content : allCountries[country].name
						}
	        		};
	        	}
	        	return result;
	        })()
	        
	    });




  });