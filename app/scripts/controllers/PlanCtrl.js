tabiApp.controller('PlanCtrl', function ($scope, planEntries, Session, $state, toastr, $rootScope) {
	$rootScope.header = "Journey Plan";
	$scope.plans = [];

	planEntries.getByUserWithPlaces(Session.userID)
		.success(function (data) {
			$scope.plans = data;
		});

	$scope.delPlanEntry = function (planId) {
		planEntries.remove(planId)
			.success(function (err) {
				toastr.success("Plan entry deleted");
				planEntries.getByUserWithPlaces(Session.userID)
					.success(function (data) {
						$scope.plans = data;
					});
			});
	}
});