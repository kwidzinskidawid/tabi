tabiApp.controller('CountryCtrl', function ($scope, $stateParams, places, journalEntries, ratings, $state, $rootScope) {
	$rootScope.header = $stateParams.name;
	$scope.places = [];
	$scope.photos = [];
	$scope.countryName = $stateParams.name;
	$scope.totalTravels = 0;
	$scope.photoToShow = undefined;

	$scope.ratings = {};
	$scope.ratings.mot = 0;
	$scope.ratings.landscape = 0;
	$scope.ratings.sig = 0;
	$scope.ratings.people = 0;
	$scope.ratings.food = 0;
	$scope.ratings.acc = 0;
	$scope.ratings.prices = 0;

	places.getByCountryName($stateParams.name)
		.success(function (data) {
			$scope.places = data;
			$scope.totalPlaces = data.length;

			if ($scope.places.length > 0) {
				journalEntries.getByCountryCode($scope.places[0].countryCode)
					.success(function (data) {
						$scope.totalTravels = data.length;
						_.each(data, function(el) {
							$scope.photos.push(el.photos);
						});

						$scope.photos = _.flatten($scope.photos);
						$scope.photos = _.last($scope.photos, 4);
					});

				ratings.getByCountryCode($scope.places[0].countryCode)
					.success(function (data) {
						_.each(data, function(el) {
								if (el.category === 'means of transport') {
									$scope.ratings.mot += el.value;
								}
								if (el.category === 'landscape') {
									$scope.ratings.landscape += el.value;
								}
								if (el.category === 'sightseeing') {
									$scope.ratings.sig += el.value;
								}
								if (el.category === 'people') {
									$scope.ratings.people += el.value;
								}
								if (el.category === 'food') {
									$scope.ratings.food += el.value;
								}
								if (el.category === 'accommodation') {	
									$scope.ratings.acc += el.value;
								}
								if (el.category === 'prices') {
									$scope.ratings.prices += el.value;
								}
						});
						$scope.ratings.mot = ($scope.ratings.mot / (data.length / 7)).toFixed(2);
						$scope.ratings.landscape = ($scope.ratings.landscape / (data.length / 7)).toFixed(2);
						$scope.ratings.sig = ($scope.ratings.sig / (data.length / 7)).toFixed(2);
						$scope.ratings.people = ($scope.ratings.people / (data.length / 7)).toFixed(2);
						$scope.ratings.food = ($scope.ratings.food / (data.length / 7)).toFixed(2);
						$scope.ratings.acc = ($scope.ratings.acc / (data.length / 7)).toFixed(2);
						$scope.ratings.prices = ($scope.ratings.prices / (data.length / 7)).toFixed(2);

					});
			} 
		});

	$scope.getVisited = function (_id, id) {
		journalEntries.getByPlace(_id)
			.success(function (data) {
				$scope.places[id].visited = data.length;
			});
	};

	$scope.getRating = function (_id, id) {
		ratings.getByPlace(_id)
			.success(function (data) {
				var average = 0;
				_.each(data, function(el) {
					average += el.value;
				});
				$scope.places[id].rating = (average / data.length).toFixed(2);
			});
	};

	$scope.redirect = function (placeName) {
		$state.go('place', {name: placeName});
	};

	$scope.showPhoto = function (photo) {
		$scope.photoToShow = photo;
		$('.photoShowWrapper').fadeIn();
	};

	$scope.hidePhoto = function () {
		$('.photoShowWrapper').fadeOut();
	};
});