tabiApp.controller('ProfileCtrl', function ($scope, places, journalEntries, Session, $rootScope) {
	$rootScope.header = "Journey Map"
	$scope.showSpinner = true;

	journalEntries.getByUserWithPlaces(Session.userID)
		.success(function (data) {
			$scope.showSpinner = false;
			var journeys = data;

			
			$(".mapcontainer").mapael({
			    map : {
			        name : "world_countries",
			        zoom : {
		                enabled : true,
		                maxLevel : 7
		            },
			        defaultArea : {
			        	attrs: {
			        		fill : "#525252",
			        		stroke : "white"
			        	},
			        	attrsHover : {
			        		fill : "#525252"
			        	}
			        	
			        }
			    },
			    areas :  (function() {
		        	var result = {};
		        	for (var country in allCountries) {
		        		result[allCountries[country].id] = {
							tooltip: {
								content : allCountries[country].name
							}
		        		};
		        	}
		        	for (var journey in journeys) {
		        		result[journeys[journey].placeID.countryCode] = {
		        			attrs: {
				        		fill : '#07a1b2'
				        	},
							tooltip: {
								content : journeys[journey].placeID.country
							}
		        		};
		        	}
		        	return result;
		        })(),
			    plots: (function() {
		        	var result = {};
		        	for (var journey in journeys) {
		        		console.log(journey);
		        		result[journeys[journey].placeID._id] = {
		        			latitude: journeys[journey].placeID.latitude,
		        			longitude: journeys[journey].placeID.longitude,
							tooltip: {
								content : journeys[journey].placeID.name + ', ' + journeys[journey].placeID.country + '<br>'
										+ new Date(journeys[journey].dateOfJourney).toISOString().slice(0, 10)
							},
							attrs : {fill : "white", opacity: 0.8},
							attrsHover : {fill : "white", opacity: 1, transform : "s1.5"},
							type: "image",
							url: "/images/located.png",
							width: 12,
							height: 40

		        		};
		        	}
		        	return result;
		        })()
			        
			});
		});

});
