tabiApp.controller('JournalEntryCtrl', function ($scope, $stateParams, journalEntries, ratings, $rootScope) {
	$rootScope.header = "Journal";
	$scope.placeName = "";
	$scope.countryName = "";
	$scope.dateOfJourney = {};
	$scope.photos = [];

	$scope.ratings = {};
	$scope.ratings.mot = 0;
	$scope.ratings.landscape = 0;
	$scope.ratings.sig = 0;
	$scope.ratings.people = 0;
	$scope.ratings.food = 0;
	$scope.ratings.acc = 0;
	$scope.ratings.prices = 0;

	journalEntries.getByIDwithPlaces($stateParams.id)
		.success(function (data) {
			if (data.length !== null ) {


				$scope.placeName = data.placeID.name;
				$scope.countryName = data.placeID.country;
				$scope.dateOfJourney = new Date(data.dateOfJourney).toISOString().slice(0, 10);
				$scope.photos = data.photos;
				$scope.description = data.description;

				_.each(data.ratings, function (r) {
					if (r.category === 'means of transport') {
						$scope.ratings.mot += r.value;
					}
					if (r.category === 'landscape') {
						$scope.ratings.landscape += r.value;
					}
					if (r.category === 'sightseeing') {
						$scope.ratings.sig += r.value;
					}
					if (r.category === 'people') {
						$scope.ratings.people += r.value;
					}
					if (r.category === 'food') {
						$scope.ratings.food += r.value;
					}
					if (r.category === 'accommodation') {	
						$scope.ratings.acc += r.value;
					}
					if (r.category === 'prices') {
						$scope.ratings.prices += r.value;
					}
				});
			}
		});

});