tabiApp.controller('PlaceCtrl', function ($scope, $stateParams, places, journalEntries, ratings, $rootScope) {
	$rootScope.header =  $stateParams.name;
	$scope.placeName = $stateParams.name;
	$scope.photos = [];
	$scope.entries = [];
	$scope.totalTravels = 0;
	$scope.photoToShow = undefined;

	$scope.ratings = {};
	$scope.ratings.mot = 0;
	$scope.ratings.landscape = 0;
	$scope.ratings.sig = 0;
	$scope.ratings.people = 0;
	$scope.ratings.food = 0;
	$scope.ratings.acc = 0;
	$scope.ratings.prices = 0;

	journalEntries.getByPlaceNameWithRatings($stateParams.name)
		.success(function (data) {
			$scope.totalTravels = data.length;

			if (data.length > 0) {
				$scope.countryName = data[0].placeID.country;
				_.each(data, function(el) {
					$scope.photos.push(el.photos);

					_.each(el.ratings, function (r) {
						if (r.category === 'means of transport') {
							$scope.ratings.mot += r.value;
						}
						if (r.category === 'landscape') {
							$scope.ratings.landscape += r.value;
						}
						if (r.category === 'sightseeing') {
							$scope.ratings.sig += r.value;
						}
						if (r.category === 'people') {
							$scope.ratings.people += r.value;
						}
						if (r.category === 'food') {
							$scope.ratings.food += r.value;
						}
						if (r.category === 'accommodation') {	
							$scope.ratings.acc += r.value;
						}
						if (r.category === 'prices') {
							$scope.ratings.prices += r.value;
						}
					});
					
					$scope.entries.push({
						description: el.description,
						dateOfJourney: new Date(el.dateOfJourney).toISOString().slice(0, 10)
					});
				});
				$scope.photos = _.flatten($scope.photos);
				$scope.photos = _.last($scope.photos, 12);

				$scope.ratings.mot = ($scope.ratings.mot / data.length).toFixed(2);
				$scope.ratings.landscape = ($scope.ratings.landscape / data.length).toFixed(2);
				$scope.ratings.sig = ($scope.ratings.sig / data.length).toFixed(2);
				$scope.ratings.people = ($scope.ratings.people / data.length).toFixed(2);
				$scope.ratings.food = ($scope.ratings.food / data.length).toFixed(2);
				$scope.ratings.acc = ($scope.ratings.acc / data.length).toFixed(2);
				$scope.ratings.prices = ($scope.ratings.prices / data.length).toFixed(2);
			}
		});

	$scope.showPhoto = function (photo) {
		$scope.photoToShow = photo;
		$('.photoShowWrapper').fadeIn();
	};

	$scope.hidePhoto = function () {
		$('.photoShowWrapper').fadeOut();
	};
});