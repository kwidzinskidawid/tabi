tabiApp.controller('TopPlacesCtrl', function ($scope, $stateParams, places, ratings, $state, $rootScope) { 
	$rootScope.header = "Top places";
	$scope.places = [];

	places.getAll()
		.success(function (data) {
			$scope.places = data;

			if ($scope.places.length > 0) {
				_.each(data, function (el, index) {
					var sum = 0;

					ratings.getByPlace(el._id)
						.success(function (data) {
							if (data.length > 0) {

								_.each(data, function (el) {
									sum += el.value;
								});

								$scope.places[index].rating = (sum / data.length).toFixed(1);
								$scope.places[index].numOfRates = data.length/7;
							}
						});
				});
			}
		});

	$scope.redirect = function (placeName) {
		$state.go('place', {name: placeName});
	};
});