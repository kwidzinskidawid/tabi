tabiApp.controller('AuthenticateCtrl', function ($scope, $state, users, places, journalEntries, Session, toastr) {
	$scope.user = {};
	$scope.newUser = {};
	$scope.searchString = "";
	$scope.user.level = "traveler";

	$('#signIn').click(function () {
		$('.logIn').show();
		$('.register').hide();
		$('.popupWrapper').fadeIn();
		$('#login').focus();
	});


	$('#menu').click(function () {
		$('.sideMenu').animate({width:'toggle'},600);
	});

	//popup
	/////////////////////////////////////

	$('.closePopup').click(function () {
		$('.popupWrapper').fadeOut();
	});

	$('.registerLink').click(function () {
		$scope.validateError = "";
		$('.register').show();
		$('.logIn').hide();
		$('#loginR').focus();
		$scope.$apply();
	});

	$('.loginLink').click(function () {
		$scope.validateError = "";
		$('.register').hide();
		$('.logIn').show();
		$('#login').focus();
		$scope.$apply();
	});


	////////////////////////// register
	$scope.register = function () {
		if ($scope.newUser.login === undefined || $scope.newUser.password === undefined || $scope.newUser.email === undefined) {
			$scope.validateError = "Enter username and password";
		} else {
			if ($scope.newUser.password === $scope.newUser.password2) {
				users.add({
					login: $scope.newUser.login,
					password: $scope.newUser.password,
					email: $scope.newUser.email})
					.success(function (data) {
						if (data.err === undefined) {
							Session.create(data.ID, data.sessionID);
							$('.popupWrapper').fadeOut();
							$scope.validateError = "";
							$scope.user.login = $scope.newUser.login;
							getLevel();
						} else {
							$scope.validateError = "Could not register";
						}
					});
			} else {
				$scope.validateError = "Passwords do not match";
			}
		}
	};

	////////////////////////// login
	$scope.login = function () {
		if ($scope.user.login === undefined || $scope.user.password === undefined) {
			$scope.validateError = "Enter username and password";
		} else {
			users.login({login: $scope.user.login, password: $scope.user.password})
				.success(function (data) {
					if (data.err === undefined) {
						Session.create(data.ID, data.sessionID);
						$('.popupWrapper').fadeOut();
						$scope.validateError = "";
						getLevel();
					} else {
						$scope.validateError = "Login or password is incorrect";
					}
				});
		}
	};

	$scope.logout = function () {
		Session.destroy();
		toastr.success("You've been logged out");
		$state.go('home');
	};

	$scope.isAuthorized = function () {
		return Session.id === undefined ? false : true;
	}

	$scope.searchPlace = function () {
		if ($scope.searchString === "") {
			toastr.error('Enter place name');
			return;
		} 
		places.getByPlaceName($scope.searchString)
			.success(function (data) {
				console.log(data);
				if (data !== null) {
					$state.go('place', {name: data.name});
				} else {
					toastr.error('No one has been there yet');
				}
			});
	}

	///////////////////////// redirect form table
	$scope.redirect = function () {
		$state.go('topPlaces');
	};

	//////////////////////// calc user level
	var getLevel = function () {
		journalEntries.getByUserWithPlaces(Session.userID)
			.success(function (data) { 
				if (data.length <= 5) {
					$scope.user.level = "traveler";
				} else if (data.length <= 20) {
					$scope.user.level = "drifter";
				} else {
					$scope.user.level = "globtrotter";
				}
			});
	}
});