tabiApp
  .controller('AddPlacePlanCtrl', function ($scope, places, planEntries, toastr, Session, $rootScope, $state) {
  	$rootScope.header = "Add place to journey plan";
  	$scope.planEntry = {};
  	$scope.planEntry.priority = 'low';

  	$rootScope.entryType = "plan";

	$scope.add = function () {
  		var placeId;

  		if ($scope.$parent.place.latitude === undefined) {
	        toastr.error('Choose the place first.');
	        return;
	      };
		places.getByCoords($scope.$parent.place.latitude, $scope.$parent.place.longitude)
			.success(function (data) {
				
			  //adding place if not found
			  if (data === "not found") {
			    places.add($scope.$parent.place)
			      .success(function(data) {
			        placeId = data;
			        console.log("PLACE ADDED" + placeId);
			        addToPlan();
			      });
			  } else {
			    placeId = data;
			    console.log("PLACE was there yo " + placeId);

			    addToPlan();
			  }
			});

			var addToPlan = function () {
				planEntries.add({
					userID: Session.userID,
					date: new Date(),
					placeID: placeId,
					priority: $scope.planEntry.priority,
					comment: $scope.planEntry.comment
				})
			    	.success(function(data) {
			            toastr.success('New journey planned!');
			            $state.go('plan');
			          });
			};


		
	};
  });