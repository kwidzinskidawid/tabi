tabiApp
  .controller('AddPlaceJournalCtrl', function ($scope, places, journalEntries, toastr, ratings, Session, $rootScope, $state) {
  	$rootScope.header = "Add place to journal";
    $scope.journalEntry = {};
    $scope.ratings = {}
    var photos = [];
    $rootScope.entryType = "journal";

    $scope.$on('flow::fileAdded', function (event, $flow, flowFile) {
      photos.push(flowFile.uniqueIdentifier);
    });

  	$scope.add = function() {
  		var placeId;
      var ratingsToAdd = [];
      console.log($scope.$parent.place);
      if ($scope.$parent.place.latitude === undefined) {
        toastr.error('Choose the place first.');
        return;
      };
      places.getByCoords($scope.$parent.place.latitude, $scope.$parent.place.longitude)
        .success(function(data) {
  		
          //adding place if not found
          if (data === "not found") {
            places.add($scope.$parent.place)
              .success(function(data) {
                placeId = data;
                console.log("First journey to this place." + placeId);
                addToJournal();
              });
          } else {
            placeId = data;
            console.log("Place already existed." + placeId);

            addToJournal();
          }
        });

      var addRating = function (value, category) {
        return function (callback) {
          var rating = {value: value, category: category, placeID: placeId};

          ratings.add(rating)
            .success(function (data) {
              ratingsToAdd.push(data);
              console.log("rating added");
              callback();
            });
        };
      };

      //adding journal entry
      var addToJournal = function() {
        if( $scope.ratings.meansOfTransport === undefined ||
            $scope.ratings.landscape === undefined ||
            $scope.ratings.food === undefined ||
            $scope.ratings.accommodation === undefined ||
            $scope.ratings.people === undefined ||
            $scope.ratings.sightseeing === undefined ||
            $scope.ratings.prices === undefined) {
              toastr.error('You have to rate all categories.');
              return;
        } else if ($scope.journalEntry.dateOfJourney == undefined) {
          toastr.error('You have to choose the date.');
          return;
        
        }
        async.parallel([
          addRating($scope.ratings.meansOfTransport, "means of transport"),
          addRating($scope.ratings.landscape, "landscape"),
          addRating($scope.ratings.food, "food"),
          addRating($scope.ratings.accommodation, "accommodation"),
          addRating($scope.ratings.people, "people"),
          addRating($scope.ratings.sightseeing, "sightseeing"),
          addRating($scope.ratings.prices, "prices")
        ], function (err) {
            journalEntries.add({
                userID: Session.userID,
                date: new Date(),
                dateOfJourney: $scope.journalEntry.dateOfJourney,
                placeID: placeId,
                description: $scope.journalEntry.description,
                ratings: ratingsToAdd,
                photos: photos
              })
              .success(function(data) {
                toastr.success('New place added to your journal!');
                $state.go('journal');
              });
            
        });  
  	};
  };
});