var mongoose = require('mongoose');
require("./Places.js");

var PlanEntrySchema = new mongoose.Schema({
	date: Date,
	placeID: {type: mongoose.Schema.Types.ObjectId, ref: 'Place'},
	priority: {
		type: String,
		enum: ["low", "medium", "high"]
	},
	comment: String,
	userID: {type: mongoose.Schema.Types.ObjectId, ref: 'User'}
});

var PlanEntry = mongoose.model("PlanEntry", PlanEntrySchema);

module.exports = PlanEntry;