var mongoose = require('mongoose');
require("./Places.js");
require("./Ratings.js");

var JournalEntrySchema = new mongoose.Schema({
	date: Date,
	dateOfJourney: Date,
	placeID: {type: mongoose.Schema.Types.ObjectId, ref: 'Place'},
	description: String,
	photos: [String],
	ratings: [{type: mongoose.Schema.Types.ObjectId, ref: 'Rating'}],
	userID: {type: mongoose.Schema.Types.ObjectId, ref: 'User'}
});

var JournalEntry = mongoose.model("JournalEntry", JournalEntrySchema);

module.exports = JournalEntry;