var mongoose = require('mongoose');
require("./Users.js");

var MessageSchema = new mongoose.Schema({
	date: Date,
	content: String,
	senderID: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
	receiverID: {type: mongoose.Schema.Types.ObjectId, ref: 'User'}
});

var Message = mongoose.model("Message", MessageSchema);

module.exports = Message;