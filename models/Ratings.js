var mongoose = require('mongoose');
require("./Places.js");

var RatingSchema = new mongoose.Schema({
	placeID: {type: mongoose.Schema.Types.ObjectId, ref: 'Place'},
	value: Number,
	category: {
		type: String,
		enum: ["means of transport", "landscape", "food", "accommodation", "people", "sightseeing", "prices"]
	}

});

var Rating = mongoose.model("Rating", RatingSchema);

module.exports = Rating; 