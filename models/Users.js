var mongoose = require('mongoose');
require("./JournalEntries.js");
require("./PlanEntries.js");

var UserSchema = new mongoose.Schema({
	level: {
		type: String,
		enum: ["traveler", "drifter", "globtrotter"],
		default: "traveler"
	},
	login: String,
	password: String,
	firstName: String,
	surname: String,
	email: String,
	admin: {type: Boolean, default: false}
});

var User = mongoose.model("User", UserSchema);

module.exports = User;