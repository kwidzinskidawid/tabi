var mongoose = require('mongoose');

var PlaceSchema = new mongoose.Schema({
	name: String,
	country: String,
	countryCode: String,
	latitude: Number, 
	longitude: Number
});

var Place = mongoose.model("Place", PlaceSchema);

module.exports = Place;